/****************************
Implement the methods specified by the Grid
interface using this data structure. Why is this
a more time-efficient implementation than BoundedGrid? 

The time required for SparseBoundedGrid is O(n + number of rows)
The time required for BoundedGrid is O(n * number of columns)

****************************/

public class SparseGridNode{
    private Object occupant;
    private int col;
    private SparseGridNode next;

    public SparseGridNode(Object occupant, int col, SparseGridNode next){
	this.occupant = occupant;
	this.col = col;
	this.next = next;
    }

    public Object getOccupant(){
	return occupant;
    }
    
    public int getColumn(){
	return col;
    }
    
    public SparseGridNode getNext(){
	return next;
    }

    public void setOccupant(Object occupant){
	this.occupant = occupant;
    }

    public void setNext(SparseGridNode next){
	this.next = next;
    }
}
