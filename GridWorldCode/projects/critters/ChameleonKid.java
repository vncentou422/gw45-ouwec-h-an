import java.util.*;
import info.gridworld.actor.Actor;
import info.gridworld.actor.Critter;
import info.gridworld.grid.Location;
import info.gridworld.grid.Grid;
import java.awt.Color;
import java.util.ArrayList;

public class ChameleonKid extends ChameleonCritter{
    public ChameleonKid(){
	super();
    }
    public ArrayList<Actor> getActors(){ //overides method in Critter.java
	ArrayList<Actor> x = new ArrayList<Actor>();
	int[] d = {Location.AHEAD/*front*/, Location.HALF_CIRCLE/*back*/}; 
	for (Location loc : getLocs(d)/*getLocs creats list of valid locations*/) 
	    { 
		Actor y = getGrid().get(loc); //adds actors who are in front and behind
		if (y != null) 
		    x.add(y); 
	    } 
 
	return x; 
    }
    public ArrayList<Location> getLocs(int[] directions) { 
	ArrayList<Location> x = new ArrayList<Location>(); 
	Grid gr = getGrid(); 
	Location loc = getLocation(); 
	
	for (int d : directions) 
	    { 
		Location neighborLoc = loc.getAdjacentLocation(getDirection() + d); 
		if (gr.isValid(neighborLoc)) 
		    x.add(neighborLoc); 
	    } 
	return x; 
    } 
    
} 	


