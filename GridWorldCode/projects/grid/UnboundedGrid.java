/* 
 * AP(r) Computer Science GridWorld Case Study:
 * Copyright(c) 2002-2006 College Entrance Examination Board 
 * (http://www.collegeboard.com).
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * @author Alyce Brady
 * @author APCS Development Committee
 * @author Cay Horstmann
 */

package info.gridworld.grid;

import java.util.ArrayList;

import java.util.*;

/**
 * An <code>UnboundedGrid</code> is a rectangular grid with an unbounded number of rows and
 * columns. <br />
 * The implementation of this class is testable on the AP CS AB exam.
 */
public class UnboundedGrid<E> extends AbstractGrid<E>{
    private Object[][] occupants;
    private Map<Location, E> occupantMap;
    private int size;

    /**
     * Constructs an empty unbounded grid.
     */
    public UnboundedGrid(){
	size = 	16;
	occupants = new Object[16][16];
        //occupantMap = new HashMap<Location, E>();
    }

    public int getNumRows()
    {
        return -1;
    }

    public int getNumCols()
    {
        return -1;
    }

    public boolean isValid(Location loc)
    {
        return loc.getRow() >= 0 && loc.getCol() > 0;
    }

    public ArrayList<Location> getOccupiedLocations()
    {
        ArrayList<Location> a = new ArrayList<Location>();
        for (Location loc : occupantMap.keySet())
            a.add(loc);
        return a;
    }

    public E get(Location loc)
    {
        if (loc == null)
            throw new NullPointerException("loc == null");
        return occupantMap.get(loc);
    }//O(1)

    public E put(Location loc, E obj)
    {
        if (loc == null)
            throw new NullPointerException("loc == null");
        if (obj == null)
            throw new NullPointerException("obj == null");
	if (loc.getRow() >= size || loc.getCol() >= size){
	    int size1 = size;;
	    while (loc.getRow() >= size1 || loc.getCol() >= size1){
		size1*=2;
	    }
	    Object[][] x = new Object[size1][size1];
	    for(int y = 0; y < size; y++)
		for(int c = 0; c < size; c++)
		    x[y][c] = occupants[y][c];
	    occupants = x;
	    size = size1;
	}
	E temp = get(loc);
	occupants[loc.getRow()][loc.getCol()] = obj;
	return temp;
        //return occupantMap.put(loc, obj);
    }//O(1) if it is in bounds         O(n^2) if array must be resized

    public E remove(Location loc)
    {
        if (loc == null)
            throw new NullPointerException("loc == null");
	if (loc.getRow() >= size || loc.getCol() >= size)
	    return null;
	E r = get(loc);
	occupants[loc.getRow()][loc.getCol()] = null;
	return r;
        //return occupantMap.remove(loc);
    }
}
