import info.gridworld.actor.*;
import info.gridworld.actor.Actor;
import info.gridworld.actor.Critter;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;
import java.util.ArrayList;

public class RockHound extends Critter{
    public void processActors(ArrayList<Actor> actors){
	for (Actor x: actors){
	    if (x instanceof Rock)
		x.removeSelfFromGrid();
	}
    }
}
