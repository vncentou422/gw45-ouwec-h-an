/*****************************************************************
 * Cooper Weaver --- OuWeC(h)an
 * APCS pd 8
 * HW35 -- GridWorld, Part 4
 * 2014-05-01
 *
 * class MunchyCritter
 *
 * BEACUASE:
 * because MunchyCritter was clearly the dopest of all the critters. It eats everything like it doesn't even care. It eats up a rock, knows its gonna die, but goes right on eating the rest of the rocks in until it reaches its imminent death. Kudos to you MunchyCritter.
 *
 * SPECIFICATIONS:
 * MunchyCritter Specs:
MunchyCritter will extend Critter. A MunchyCritter looks at all of the neighbors within one step of its current location. It will then proceed to eat (remove) all actors within one stepo of its current location. However, if the actor that is removed is a Rock, then the MunchyCritter dies. If there are no actors within a one step radius of its current location, the MunchyCritter will move in a random direction for one step.
 *
 * TEST CASES:
 *MunchyCritter Test Cases:
First Test Case: A MunchyCritter is placed in a grid where there are only actors except for Rocks. 
The MunchyCritter would be able to eat anything in its radius and keep moving if there is nothing to eat.
Second Test Case: A MunchyCritter is placed in a grid where there are only actors that are Rocks.
The MunchyCritter move around and then if it encounters a rock, it will eat and then proceed to "die" or remove itself.
Third Test Case: A MunchyCritter is placed at the edge of the grid.
It should be able to discern which direction is a valid location to step in and proceed to step in the correct path.
Fourth Test Case: When the MunchyCritter is surrounded by Actors other than Rocks.
All Actors should be consumed.
Fifth Test Case: When the MunchyCritter is surrounde by Rocks
All rocks should be consumed and the MunchyCritter will remove itself. 
 *
 * ERRATA:
 * It was not specified what would happen when two MunchyCritters met eachother, and while it did see appropriate to have an epic battle to the death that could possibly cause the end of the world as we know it... we decided against that when we realised that one of the MunchyCritters should the other, and one should survive. You can imagine the Epicness of the battle.
 * 
 *****************************************************************/
import info.gridworld.actor.*;
import info.gridworld.grid.*;
import java.util.*;

public class MunchyCritter extends Critter{
    public void act(){
	if (getGrid() == null)
            return;
        ArrayList<Actor> actors = getActors();
        processActors(actors);
        if (getGrid() == null)
            return;
	ArrayList<Location> moveLocs = getMoveLocations();
        Location loc = selectMoveLocation(moveLocs);
        makeMove(loc);
    }

    public void processActors(ArrayList<Actor> actors){
	boolean ateRock = false;
	for(Actor a : actors){
	    if(a instanceof Rock){
		ateRock = true;
	    }
	    a.removeSelfFromGrid();
	}
	if(ateRock){
	    removeSelfFromGrid();
	}
    }
}
