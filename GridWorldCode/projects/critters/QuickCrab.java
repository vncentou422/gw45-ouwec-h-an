
import info.gridworld.actor.Actor;
import info.gridworld.actor.Critter;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;
import java.util.ArrayList;

public class QuickCrab extends CrabCritter{
    public ArrayList<Location> getMoveLocations(){
	ArrayList<Location> x = new ArrayList<Location>();
	Grid g = getGrid();
	Location loc = getLocation();
	Location t = loc.getAdjacentLocation(Location.LEFT);
	Location t1 = loc.getAdjacentLocation(Location.RIGHT);
	int dir = getDirection() + Location.LEFT;
	int dir1 = getDirection() + Location.RIGHT;
	if (g.isValid(t) && g.get(t) == null){
	    Location loc2 = t.getAdjacentLocation(dir);
	    if(g.isValid(loc2) && g.get(loc2) == null)
		x.add(loc2);
	}
	if (g.isValid(t1) && g.get(t1) == null){
	    Location loc2 = t1.getAdjacentLocation(dir1);
	    if(g.isValid(loc2) && g.get(loc2) == null)
		x.add(loc2);
	}
	return x;
    }
}
