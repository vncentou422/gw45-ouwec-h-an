
import info.gridworld.actor.Actor;
import info.gridworld.actor.Critter;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;
import java.util.ArrayList;

public class KingCrab extends CrabCritter{
    public boolean canMoveAway(Actor x){
	ArrayList<Location> locs = getGrid().getEmptyAdjacentLocations(x.getLocation());
	for (Location loc: locs){
	    if(distance(getLocation(), loc) > 1){
		x.moveTo(loc);
		return true;
	    }
	}
	return false;
    }
    public void processActors(ArrayList<Actor> actors){
	for (Actor x: actors){
	    if (!canMoveAway(x))
		x.removeSelfFromGrid();
	}
    }
    public int distance(Location x, Location y){
	int xcor1 = x.getRow(); int ycor1 = x.getCol();
	int xcor2 = y.getRow(); int ycor2 = y.getCol();
	return (int)(Math.sqrt(((xcor1 - xcor2)* (xcor1 - xcor2)) + (ycor1 - ycor2) * (ycor1 - ycor2)));
    }
}
