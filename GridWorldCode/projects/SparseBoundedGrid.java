/****************************
Implement the methods specified by the Grid
interface using this data structure. Why is this
a more time-efficient implementation than BoundedGrid? 

The time required for SparseBoundedGrid is O(n + number of rows)
The time required for BoundedGrid is O(n * number of columns)

***************************/

import info.gridworld.grid.Grid;
import info.gridworld.grid.AbstractGrid;
import info.gridworld.grid.Location;

import java.util.ArrayList;

public class SparseBoundedGrid<E> extends AbstractGrid<E>{
    private SparseGridNode[] occupants;
    private int cols;
    private int rows;

    public SparseBoundedGrid(int rows, int cols){
	if(rows <= 0 || cols <= 0){
	    throw new IllegalArgumentException("Rows or Cols <= 0");
	}
	this.cols = cols;
	this.rows = rows;
	occupants = new SparseGridNode[this.rows];
    }
    
    public int getRows(){
	return rows;
    }

    public int getCols(){
	return cols;
    }

    public boolean isValid(Location loc){
	return 0 <= loc.getRow() && loc.getRow() < getRows() && 0 <= loc.getCol() && loc.getCol() < getCols();
    }

    public ArrayList<Location> getOccupiedLocations(){
	ArrayList<Location> locations = new ArrayList<Location>();

	for(int row = 0; row < getRows(); row ++){
	    SparseGridNode node = occupants[row];
	    while(node != null){
		Location loc = new Location(row, node.getColumn());
		locations.add(loc);
		node = node.getNext();
	    }
	}
	return locations;
    }

    public E get(Location loc){
	if(!isValid(loc)){
	    throw new IllegalArgumentException("Location is not valid");
	}

	SparseGridNode node = occupants[loc.getRow()];
	while(node != null){
	    if(loc.getCol() == node.getColumn()){
		return (E)node.getOccupant();
	    }
	    node = node.getNext();
	}
	
	return null;
    }

    public E put(Location loc, E object){
	if(!isValid(loc)){
	    throw new IllegalArgumentException("Location is not valid");
	}

	if(object == null){
	    throw new NullPointerException("object is null");
	}

	E old = remove(loc);

	SparseGridNode node = occupants[loc.getRow()];
	
	occupants[loc.getRow()] = new SparseGridNode(object, loc.getCol(), node);

	return old;
    }
    
}
