import info.gridworld.actor.*;
import info.gridworld.grid.*;

import java.awt.Color;

public class MunchyCritterDriver{
    public static void main(String[] args){
        ActorWorld world = new ActorWorld();
        Critter munchy = new MunchyCritter();
        Critter munchy1 = new MunchyCritter();
        Critter munchy2 = new MunchyCritter();
        Critter munchy3 = new MunchyCritter();
        Critter munchy4 = new MunchyCritter();
        Actor rock = new Rock();
        Actor rock1 = new Rock();
        Actor rock2 = new Rock();
        Actor rock3 = new Rock();
        Actor rock4 = new Rock();
        Actor rock5 = new Rock();
        Actor flower = new Flower();
        Actor bug = new Bug();
        world.add(new Location(0, 0), munchy);
        world.add(new Location(0, 1), rock);

	world.add(new Location(0, 9), munchy1);
	world.add(new Location(0, 8), rock1);
	world.add(new Location(1, 8), rock2);
	world.add(new Location(1, 9), rock3);

	world.add(new Location(9, 0), munchy2);
	world.add(new Location(8, 0), rock4);
	world.add(new Location(8, 1), flower);
	
	world.add(new Location(9, 9), munchy3);

	world.add(new Location(5,5), munchy4);
	world.add(new Location(4,5), bug);

	world.show();
    }
}
