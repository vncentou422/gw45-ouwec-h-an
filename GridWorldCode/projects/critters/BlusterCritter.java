
import info.gridworld.actor.Actor;
import info.gridworld.actor.Critter;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;
import java.util.ArrayList;

public class BlusterCritter extends Critter{
    private int courage;
    public BlusterCritter(int c){
	super();
	courage = c;
    }
    public ArrayList<Actor> getActors(){
        ArrayList<Actor> adj = getGrid().getNeighbors(getLocation());
        ArrayList<Actor> actors = new ArrayList<Actor>();
        for( Actor a : adj ){
            ArrayList<Actor> n = getGrid().getNeighbors(a.getLocation());
            for(Actor b : n){
                if(!b.equals(this) && !actors.contains(b) && !adj.contains(b)){
                    actors.add(b);
                }
            }
            actors.add(a);
        }
        return actors;
    }
    public void processActors(ArrayList<Actor> actors){
	int x = 0;
	for (Actor y: actors){
	    if (y instanceof Critter)
	        x++;
	
	}
	if (x < courage)
	    light();
	else
	    dark();
    }
    public void light(){
	
	Color x = getColor();
	int red = (x.getRed());
	int green = (x.getGreen()); 
	int blue = (x.getBlue());
	if (red < 255) 
	    red++;
	if (green < 255) 
	    green++;
	if (blue < 255)
	    blue ++;
	setColor(new Color(red,green,blue));
		
    }
    public void dark(){
	Color x = getColor();
	int red = (int)(x.getRed() * (1 - .5));
	int green = (int)(x.getGreen() * (1 - .5));
	int blue = (int)(x.getBlue() * (1 - .5));
	setColor(new Color(red,green,blue));
    }
    
}
